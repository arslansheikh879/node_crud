const mysql = require('mysql');

const express = require('express');
var app = express() 
const bodyparser = require('body-parser');

app.use(bodyparser.json());

var mysqlConnection = mysql.createConnection({
	host:'localhost',
	user: 'root',
	password: '',
	database:'employeedb'
});


mysqlConnection.connect((err) =>  {
	if (!err) {
		console.log('DB connection Successfully');
	}
		else 
			console.log('Db connection Error \n Error :' + JSON.stringify(err, undefined, 2));
	
})

app.listen(3000,() => console.log('Express server is running at port no : 3000  '));

app.get('/employes', (req,res) => {
mysqlConnection.query('SELECT * FROM employee',(err,rows,fields) =>{
if(!err){
res.send(rows);
}
else 
	console.log(err);
});

});


app.get('/employes/:id', (req,res) => {
mysqlConnection.query('SELECT * FROM employee where EmpID = ?',[req.params.id],(err,rows,fields) =>{
if(!err){
res.send(rows);
}
else 
	console.log(err);
});

});

app.delete('/employes_delete/:id', (req,res) => {
mysqlConnection.query('DELETE FROM employee where EmpID = ?',[req.params.id],(err,rows,fields) =>{
if(!err){
res.send('Deleted Successfully');
}
else 
	console.log(err);
});

});

app.post('/employee_add', (req,res) => {
	console.log(req);
	
	var post  = {Name: 'Haris',EmpCode: 'haris-09',Salary:'100000'};
mysqlConnection.query('INSERT INTO employee SET ?', post, function (error, results, fields) {
if (error){
	console.log(error);
}

else 
res.send('Add Successfully');

});

});
